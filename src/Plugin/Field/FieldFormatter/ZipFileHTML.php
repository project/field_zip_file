<?php

namespace Drupal\field_zip_file\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Formatter that shows the file size in a human readable way.
 *
 * @FieldFormatter(
 *   id = "zip_file_html",
 *   label = @Translation("HTML Files"),
 *   field_types = {
 *     "zip_file"
 *   }
 * )
 */
class ZipFileHTML extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => 'test'];
    }

    return $elements;
  }

}
